FROM node:8-alpine

WORKDIR /home/node

RUN chown -R node:node .
COPY --chown=node:node src src
COPY --chown=node:node typescript/server/index.js src/server/index.js
COPY --chown=node:node package.json package.json
COPY --chown=node:node key.pem key.pem
COPY --chown=node:node certificate.pem certificate.pem

USER node
RUN npm install && npm cache clean --force

EXPOSE 3000
CMD [ "npm", "start" ]