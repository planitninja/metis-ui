'use strict';
import * as React from 'react';
import { Component } from 'react';
import { II18n } from '../core/i18n';
let css = require('./style.css');


export class MissingModule extends Component<{}, { i18n: II18n }>
{
    constructor(props: {}, context: {}) {
        super(props, context);
        this.state = {
            i18n: {
                get: (key) => key
            }
        }

    }


    render() {
        let { i18n } = this.state;
        return (
            <div className="main">
                {i18n.get('module-not-found')}
            </div>
        );
    }
}
