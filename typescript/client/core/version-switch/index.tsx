'use strict';
import * as React from 'react';
import { Component } from 'react';
import { UnauthorisedError } from '../../errors';
import { ISecuredRequest, IUserProfile, SecuredRequest, SecurityContext } from '../access-control';

export interface IAuthorisedComponent {
    feature: string;
    versionMap: (version: string) => any;
}

class VersionSwitchComponent extends Component<IAuthorisedComponent, {}>
{
    _securedRequest: ISecuredRequest
    constructor(props: IAuthorisedComponent, context: {}) {
        super(props, context);
        this._securedRequest = new SecuredRequest();
    }


    versionSwitch(profile: IUserProfile) {
        if(profile == null) {
            this._securedRequest.authenticate();
            return (<div></div>);
        }
        let version = profile.featureVersion(this.props.feature);
        if(version) {
            let versionModule = this.props.versionMap(version);
            if(versionModule) {
                return versionModule;
            }
        }
        return (<UnauthorisedError/>);
    }


    render() {
        return (
            <SecurityContext.Consumer>
                {this.versionSwitch.bind(this)}
            </SecurityContext.Consumer>);
    }
}

export default VersionSwitchComponent;