'use strict';
import * as React from 'react';
import { Component } from 'react';
import * as PropTypes from 'prop-types';
import { ValidationError } from 'metis-errors';
import { IFieldSchema } from '@metis/model-validation';
import { formControlBuilder } from './form-control-builder';
import { IControlProperties } from './control-properties';
import * as extend from 'extend';

export interface IValidatedControlProperties extends IControlProperties {
    showError?: boolean,
    schema: IFieldSchema,
    onValueChanged?: (value:string) => void;
    name: string;
};

interface IValidatedControlState {
    error: any,
    value: any,
    showError: boolean
};

/**
 * Provides skeleton for displaying label and input field of ane types.
 */
class ValidatedControl extends Component<IValidatedControlProperties, IValidatedControlState>
{
    controlProperties: any;
    constructor(props: IValidatedControlProperties, context: {}) {
        super(props, context);
        this.controlProperties = extend(true, {}, this.props);
        delete this.controlProperties.showError;
        delete this.controlProperties.schema;

        this.state = {
            error: null,
            value: this.props.value,
            showError: this.props.showError == undefined ? true : this.props.showError
        }
    }

    classNames() {
        var base = "form-group";
        return this.state.error ? base + " has-error" : base;
    }

    validationStateChanged(error: ValidationError) {
        this.setState({ error });
    }


    _createChildControl() {
        return formControlBuilder(this.props.name, this.props.schema, this.validationStateChanged.bind(this), this.controlProperties);
    }

    render() {
        const { showError, error} = this.state;
        return (
            <div className={this.classNames()}>
                {this._createChildControl()}
                {showError && error &&
                    <div className="alert alert-danger">
                        <span>{error.message}</span>
                    </div>
                }
            </div>
        );
    }
}

export default ValidatedControl;