import { Component } from 'react';
import { ValidationError } from 'metis-errors';
import { IFieldEvaluator } from '@metis/model-validation';
import * as extend from 'extend';

export interface IValidatedBaseControlProperties {
    onValidationFailed: (error: ValidationError) => void,
    onValueChanged: (value: string) => void,
    constraint: IFieldEvaluator;
}

class ValidatedBaseControl<TProperties extends IValidatedBaseControlProperties, TState extends { value: string }, TContext> extends Component<TProperties, TState> {
    controlProperties:any;

    constructor(props: TProperties, context: TContext) {
        super(props, context);
        this.controlProperties = extend(true,{},this.props);
        delete this.controlProperties.constraint;
    }

    _validate(value: string): string {
        let onValidationFailed = this.props.onValidationFailed;
        if (this.props.constraint) {
            let result = this.props.constraint.tryValidateValue(value);
            onValidationFailed && onValidationFailed(result.error);
            return result.value;
        }
        return value;
    }

    _setValue(newValue: string) {
        let onValueChanged = this.props.onValueChanged;
        let value = this._validate(newValue);
        this.setState({
            value
        });
        onValueChanged && onValueChanged(value);
    }
}

export { ValidatedBaseControl };