import * as React from 'react';
import { Component } from 'react';
import { ValidatedBaseControl, IValidatedBaseControlProperties } from "./validation-base";

export interface ITextControlProperties extends IValidatedBaseControlProperties {
    value: string
}

class TextControl extends ValidatedBaseControl<ITextControlProperties, { value: string, name: string}, {}> {

    constructor(props: ITextControlProperties, context: {}) {
        super(props, context);
        this.state = {
            value: props.value || '',
            name: props.constraint.name
        }
    }

    handleChange(event: any) {
        this._setValue(event && event.target ? event.target.value : event);
    }

    handleBlur(event: any) {
        this._setValue(event && event.target ? event.target.value : event);
    }

    render() {
        let { name } = this.state;
        let properties = this.controlProperties;
        return (<input
            id={name}
            onChange={this.handleChange.bind(this)}
            onBlur={this.handleBlur.bind(this)}
            {...properties} />);
    }
}


export { TextControl };