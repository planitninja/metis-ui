export interface IControlProperties{
    placeholder?: string;
    className?: string;
    type?: string;
    required?: boolean,
    autoFocus?: boolean,
    value?: any,
    autoComplete?: string
}