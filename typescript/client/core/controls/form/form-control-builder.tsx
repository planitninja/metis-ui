import { IFieldSchema, FieldTypes, FieldEvaluator } from '@metis/model-validation';
import { TextControl } from "./text-control";
import * as React from 'react';
import { Component } from 'react';
import { ValidationError } from "metis-errors";

export function formControlBuilder(name:string, schema: IFieldSchema, onValidationFailed: (error: ValidationError) => void, properties:any) {

    let fieldEvaluator = new FieldEvaluator(name, schema);
    return (<TextControl constraint={fieldEvaluator} onValidationFailed={onValidationFailed} {...properties}/>);
}