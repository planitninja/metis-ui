export default {
    apiHost : 'https://api.planitninja.com',
    clientId: 'clientId',
    userProfileKey: 'metis_userProfile',
    tokenKey: 'metis_token',
    userNameKey: 'metis_username',
    isDev: true
}