'use strict';
import * as React from 'react';
import { Component } from 'react';
import LoadingIndicator from '../loading-indicator';
import SecurityContext from './security-context';
import { IUserProfile, IProfileData, UserProfile } from './user-profile';
import {SecuredRequest } from './secured-request';
import constants from './const';

class SecuredComponent extends Component<{}, {profile: IUserProfile, isLoading: boolean}>
{
    constructor(props: {}, context: {}) {
        super(props, context);
        let userProfile = sessionStorage.getItem(constants.userProfileKey);
        if(userProfile) {
            this.state = {
                profile: new UserProfile(JSON.parse(userProfile)),
                isLoading: false
            }
        }
        else {
            this.state = {
                profile: null,
                isLoading: true
            }
            this.userProfile().then(profile => {
                if(profile) {
                    this.setState({profile, isLoading: false});
                }
            }); 
        }
    }

    userProfile(): Promise<IUserProfile> {
        let securedRequest = new SecuredRequest(null);
        return securedRequest.fetch<IProfileData>('openid-provider', '/me', { method: 'GET' })
        .then(userProfile => {
            if(userProfile) {
                sessionStorage.setItem(constants.userProfileKey, JSON.stringify(userProfile));
                return new UserProfile(userProfile);
            }
            return null;
        });
    }


    render() {
        if(this.state.isLoading) {
            return (<LoadingIndicator />);
        }
        return (
            <SecurityContext.Provider value={this.state.profile}>
                {this.props.children}
            </SecurityContext.Provider>);
    }
}

export default SecuredComponent;