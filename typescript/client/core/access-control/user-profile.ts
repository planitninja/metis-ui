export interface IUserProfile {
    name:string;
    featureEnabled(feature: string):boolean;
    featureVersion(feature: string):string;
    service(serviceName: string): IServiceData;
}


export interface IServiceData {
    isAvailable: boolean,
    mode: string
}

export interface IProfileData {
    name:string,
    features: {[index:string]:string}
}

export class UserProfile implements IUserProfile{

    service(serviceName: string): IServiceData {
        return {
            mode: 'dev',
            isAvailable: true
        }
    }

    _profileData: IProfileData;
    constructor(profileData: IProfileData) {
        this._profileData = profileData;
        this._profileData.features = this._profileData.features || {};
    }

    get name() {
        return this._profileData.name;
    }

    featureEnabled(feature: string): boolean {
        return this._profileData.features[feature] && true;
    }

    featureVersion(feature: string): string {
        return this._profileData.features[feature] || null;
    }
}