import extend = require("extend");
import { HostName } from '../host';
import * as crypto from 'crypto';
import constants from './const';
import { IUserProfile } from ".";

interface ISecuredRequest {
    authenticate(): void;
    fetch<T>(serviceName: string, path: string, request: RequestInit): Promise<T>
}


export interface ITokenData {
    state?: string,
    accessToken?: string,
    idToken?: string,
    expiresIn?: string;
    type?: string;
    now: number;
}


class SecuredRequest implements ISecuredRequest {
    _apiHost: string;
    _isDev: boolean;
    _profile: IUserProfile;
    _clientId: string = constants.clientId;

    constructor(profile: IUserProfile) {
        this._apiHost = constants.apiHost;
        this._profile = profile;
        this._isDev = constants.isDev;
    }

    _url(serviceName: string, path: string) {
        if (path.startsWith('/') == false) {
            path = '/' + path;
        }
        if (this._profile) {
            let service = this._profile.service(serviceName);
            if (service) {
                if(!service.isAvailable) {
                    throw new Error('Service is not available');
                }
                return `${this._apiHost}/${service.mode}/${serviceName}${path}`;
            }
        }
        if (this._isDev) {
            return `${this._apiHost}/dev/${serviceName}${path}`;
        }
        return `${this._apiHost}/${serviceName}${path}`;
    }

    _requestWithErrorHandling(url: string, request: ResponseInit) {
        return fetch(url, request).then(result => {
            if (result.ok) {
                return result.json();
            }
            if (result.status == 401) {
                this.authenticate();
            }
            console.log('Error!');
        });
    }

    fetch<T>(serviceName: string, path: string, request: RequestInit): Promise<T> {
        let url = this._url(serviceName, path);
        let rawToken = sessionStorage.getItem(constants.tokenKey);
        let token: ITokenData = rawToken && JSON.parse(rawToken);
        if (token) {
            let type = token.type || 'Bearer';
            let authorization = `${type} ${token.accessToken}`
            request.headers = extend({}, request.headers, { authorization });
        }
        return this._requestWithErrorHandling(url, request);
    }

    authenticate() {
        let nonce = crypto.randomBytes(8).toString('base64');
        let stateId = crypto.randomBytes(8).toString('hex');
        let hint = localStorage.getItem(constants.userNameKey) || null;
        let state = {
            nonce,
            redirect: window.location.href
        };
        sessionStorage.setItem(stateId, JSON.stringify(state));
        let url = this._url('openid-provider', '/auth');
        url = `${url}?client_id=${this._clientId}&response_type=id_token+token&redirect_uri=${HostName}/auth/callback&nonce=${nonce}&state=${stateId}&scope=openid+email+profile`
        if (hint) {
            url = `${url}&login_hint=${hint}`;
        }
        return window.location.assign(url);
    }
}

export { ISecuredRequest, SecuredRequest };