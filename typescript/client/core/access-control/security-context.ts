import * as React from 'react';
import { IUserProfile } from './user-profile';

const SecurityContext = React.createContext<IUserProfile>(null);
export default SecurityContext;