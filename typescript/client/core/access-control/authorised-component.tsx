'use strict';
import * as React from 'react';
import { Component } from 'react';
import { IUserProfile, } from './user-profile';
import {SecuredRequest, ISecuredRequest } from './secured-request';
import SecurityContext from './security-context';
import { UnauthorisedError } from '../../errors';

export interface IAuthorisedComponent {
    feature: string;
    trim?: boolean;
}

class AuthorisedComponent extends Component<IAuthorisedComponent, {}>
{
    _securedRequest: ISecuredRequest
    constructor(props: IAuthorisedComponent, context: {}) {
        super(props, context);
        this._securedRequest = new SecuredRequest();
    }


    authorise(profile: IUserProfile) {
        if(profile == null) {
            this._securedRequest.authenticate();
            return (<div></div>);
        }
        if(profile.featureEnabled(this.props.feature)) {
            return this.props.children;
        }
        return this.props.trim ?
            (<div/>) : (<UnauthorisedError/>);
    }


    render() {
        return (
            <SecurityContext.Consumer>
                {this.authorise.bind(this)}
            </SecurityContext.Consumer>);
    }
}

export default AuthorisedComponent;