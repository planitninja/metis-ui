
import * as React from 'react';
import { Component } from 'react';
import { IUserProfile, Constants } from './access-control';
import { i18n, II18n } from './i18n';
import { UserProfile } from './access-control/user-profile';

export interface IMetisContext {
    userProfile: IUserProfile,
    i18n: II18n
}

const MetisContext = React.createContext<IMetisContext>(null);


class MetisContextComponent extends Component<{}, IMetisContext>
{
    constructor(props: {}, context: {}) {
        super(props, context);
        let profileData = sessionStorage.getItem(Constants.userProfileKey);
        let userProfile = profileData && new UserProfile(JSON.parse(profileData));
        let _i18n = new i18n();
        this.state = {
            i18n: _i18n,
            userProfile
        }
    }

    render() {
        return (
            <MetisContext.Provider value={this.state}>
                {this.props.children}
            </MetisContext.Provider>);
    }
}

export { MetisContext, MetisContextComponent };