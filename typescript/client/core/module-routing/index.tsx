'use strict';
import { withRouter, Route, RouteComponentProps } from 'react-router-dom'
import * as React from 'react';
import { PureComponent, Component } from 'react';
import LoadingIndicator from '../loading-indicator';
import { MissingModule } from '../../errors';
const map = require('../../modules/component-map.js').default;

interface LazyLoadingProps {
  component: () => Promise<any>
  location? : any
}

class LazyLoader extends PureComponent<LazyLoadingProps, { AsyncComponent: any }> {
  constructor(props: LazyLoadingProps) {
    super(props);

    this.state = {
      AsyncComponent: null
    }
  }

  componentWillMount() {
    if (!this.state.AsyncComponent) {
      this.props.component()
        .then((mod) => {
          if (mod) {
            this.setState({ AsyncComponent: mod });
          }
          else {
            this.setState({ AsyncComponent: MissingModule });
          }
        }).catch(error => {
          this.setState({ AsyncComponent: MissingModule });
        });
    }
  }

  render() {
    const { AsyncComponent } = this.state;
    return (
      <div>
        {AsyncComponent ? <AsyncComponent location={this.props.location} /> : <LoadingIndicator />}
      </div>
    );
  }
};


export interface IFeatureRouteProps  extends RouteComponentProps {
    path: string;
    module: string;
}

class FeatureRoute extends Component<IFeatureRouteProps, {}>
{
    constructor(props: IFeatureRouteProps, context: {}) {
        super(props, context);

        this.state = {
        }
    }

    component() {
        return map[this.props.module]()
            .then( (module : any) => {
                return module.default;
            });
    }

    render() {
        let path = this.props.path;
        return (
            <Route exact path={path} component={() => (<LazyLoader component={this.component.bind(this)} />)} />
        );

    }
}

export default withRouter(FeatureRoute);