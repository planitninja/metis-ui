import { ISecuredRequest, SecuredRequest } from "./access-control";

export interface II18n {
    get(section: string, key: string ): Promise<string>;
}



export class i18n implements II18n {
    _map: Map<string, {[index: string] : string}>;
    _request: ISecuredRequest;
    constructor() {
        this._request = new SecuredRequest(null);
        this._map = new Map<string, {[index: string] : string}>();
    }

    async get(section: string, key: string ): Promise<string>{
        if(this._map.has(section) == false) {
            let data = await this._request.fetch<{[index: string] : string}>('localisation', `/sections/${section}`, {method: "GET"});
            data = data || {};
            this._map.set(section, data);
        }

        let sectionData = this._map.get(section);
        let value = sectionData && sectionData[key];
        return value || `***${section}.${key}`;
    }
}