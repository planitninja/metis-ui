

function BuildHostName() {
    let protocol = window.location.protocol || 'http:'
    if(window.location.port && window.location.port != "80" && window.location.port != "443") {
        return `${protocol}//${window.location.hostname}:${location.port}`;
    }
    return `${protocol}//${window.location.hostname}`;
}


export const HostName = BuildHostName();