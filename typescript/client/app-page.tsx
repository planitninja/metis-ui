'use strict';
import * as React from 'react';
import { Component } from 'react';
import {  SecurityContext, IUserProfile } from './core/access-control';


class AppPage extends Component<{}, {}>
{
    constructor(props: {}, context: {}) {
        super(props, context);
    }

    welcome(profile: IUserProfile) {
        return (
            <div>
                This is the application {profile.name}
            </div>
        );
    }

    render() {

        return (
                <SecurityContext.Consumer>
                    {this.welcome}
                </SecurityContext.Consumer>
        );
    }
}


export default AppPage;