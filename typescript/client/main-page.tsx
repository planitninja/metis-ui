'use strict';
import * as React from 'react';
import { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import ModuleRoute from './core/module-routing';
import AppPage from './app-page';
import HeaderComponent from './modules/header';
import { MetisContextComponent } from './core/metis-context';

let css = require('./main.css');
class MainPage extends Component<{}, {}>
{
    constructor(props: {}, context: {}) {
        super(props, context);
    }


    render() {

        return (
            <MetisContextComponent>
                <HeaderComponent>
                    
                </HeaderComponent>
                <section>
                    <nav>
                        <div className="fixSizing">
                            <ul>
                                <li><a href="#">London</a></li>
                                <li><a href="#">Paris</a></li>
                                <li><a href="#">Tokyo</a></li>
                            </ul>
                        </div>
                    </nav>
                    <article>
                        <div className="fixSizing">
                            <BrowserRouter>
                                <Switch>
                                    <ModuleRoute path="/calendar" module="calendar" />
                                    <Route path="/" component={AppPage}/>
                                </Switch>
                            </BrowserRouter>
                        </div>
                    </article>
                </section>
                <footer>
                    <p>Footer</p>
                </footer>
            </MetisContextComponent>
        );
    }
}

export default MainPage;
