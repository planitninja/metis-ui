'use strict';
import * as React from 'react';
import { Component } from 'react';
import { MetisContext, IMetisContext } from '../../core/metis-context';
import { IUserProfile } from '../../core/access-control';
let css = require('./style.css');


class HeaderComponent extends Component<{}, {}>
{
    constructor(props: {}, context: {}) {
        super(props, context);
    }

    profile(userProfile: IUserProfile) {
        if(userProfile) {
            return (<div className="menu_profile">
                {userProfile.name}
            </div>);
        }
        else {
            return (<div className="menu_profile">
                Login
            </div>);
        }
    }

    menuToggle() {
        return (
            <div className="menu_toggle">
                <button type="button">
                    <span>Toggle navigation</span>
                </button>
            </div>);
    }

    header(context: IMetisContext) {
        return (<header className="main_header">
            <button type="button">
                <span>Toggle navigation</span>
            </button>
            {this.profile(context.userProfile)}
        </header>);
    }

    render() {
        return (
            <MetisContext.Consumer>
                {this.header.bind(this)}
            </MetisContext.Consumer> 
        );
    }
}

export default HeaderComponent;
