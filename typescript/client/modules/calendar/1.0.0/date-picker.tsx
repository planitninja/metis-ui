'use strict';
import * as React from 'react';
import { Component } from 'react';
import { IGridDate, ICalendarLocale } from './grid-date';


export interface IDatePickerProperties {
    date: IGridDate
    dateChanged?: (date: IGridDate) => void;
    calendarLocale: ICalendarLocale;
}

class DatePicker extends Component<IDatePickerProperties, {current: IGridDate}>
{
    _calendarLocale: ICalendarLocale;
    constructor(props: IDatePickerProperties, context: {}) {
        super(props, context);
        this._calendarLocale = props.calendarLocale;
        this.state = {
            current : props.date
        }
    }

    static getDerivedStateFromProps(props: IDatePickerProperties, state: {current: IGridDate}) {

        if (state == null || state.current == null || !state.current.equals(props.date)) {
            return {
                current: props.date
            };
        }
        return null;
    }


    onDateChanged(current: IGridDate) {
        if(this.props.dateChanged) {
            this.props.dateChanged(current);
        }
    }

    next() {
        let current = this.state.current.add({years: 1});
        this.setState({current});
        this.onDateChanged(current);
    }

    nextMonth() {
        let current = this.state.current.add({months: 1});
        this.setState({current});
        this.onDateChanged(current);
    }

    prev() {
        let current = this.state.current.remove({years: 1});
        this.setState({current});
        this.onDateChanged(current);
    }

    prevMonth() {
        let current = this.state.current.remove({months: 1});
        this.setState({current});
        this.onDateChanged(current);
    }

    dateDisplay(current: IGridDate) {
        return `${this._calendarLocale.monthName(current.month)}  ${current.year}`;
    }

    render() {
        let { current } = this.state;
        return (
            <div className="date-picker header corner-all">
                <a title="Prev" className="button corner-all previous" onClick={this.prev.bind(this)}><span className="glyphicon glyphicon-triangle-left left-aligned">a</span></a>
                <a title="Prev" className="button corner-all monthControlsPrevious" onClick={this.prevMonth.bind(this)}><span className="glyphicon glyphicon-backward left-aligned">a</span></a>
                <a title="Next" className="button monthControlsNext corner-all" onClick={this.nextMonth.bind(this)}><span className="glyphicon glyphicon-forward right-aligned">a</span></a>
                <a title="Next" className="button corner-all next" onClick={this.next.bind(this)}><span className="glyphicon glyphicon-triangle-right right-aligned">a</span></a>
                <div className="title">
                    <span>{this.dateDisplay(current)}</span>
                </div>
            </div>
        );
    }
}

export default DatePicker;
