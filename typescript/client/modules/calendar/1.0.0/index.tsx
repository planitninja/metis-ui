'use strict';
import * as React from 'react';
import { Component } from 'react';
import { II18n } from '../../../core/i18n';
import CalendarService, { ITimetable, IEventType, ITimetableEvent } from './api';
import DatePicker from './date-picker';
import CalendarGrid from './calendar-grid';
import Date, { ICalendarLocale, IGridDate } from './grid-date';
import EventList, { IEventMap } from './event-list';
import { IUserProfile, SecurityContext } from '../../../core/access-control';
let css = require('./style.css');
const minWidth = 1000;

interface ICalendarState { 
    events: IEventMap, 
    isLoaded: boolean, 
    current:IGridDate, 
    showGrid:boolean 
}

class EventMap implements IEventMap{
    _map: {[index: string]: {event: ITimetableEvent, type: IEventType}[]} = {};

    _getKey(date: IGridDate) {
        return `${date.month}-${date.date}`;
    }

    hasEvents(date: IGridDate): boolean {
        return this._map[this._getKey(date)] && true;
    }    
    
    getEvents(date: IGridDate): { event: ITimetableEvent; type: IEventType; }[] {
        return this._map[this._getKey(date)] || null;
    }
    addEvent(date: IGridDate, event: ITimetableEvent, type: IEventType): void {
        let key = this._getKey(date);
        if(this._map[key]) {
            this._map[key].push({event, type});
        }
        else {
            this._map[key] = [{event, type}];
        }
    }
}

class Calendar extends Component<{},ICalendarState >
{
    _locale: ICalendarLocale;
    _eventTypes: IEventType[];

    constructor(props: {}, context: {}) {
        super(props, context);
        this.state = {
            events: null,
            isLoaded: false,
            current: new Date().toGridDate(),
            showGrid: window.innerWidth >= minWidth
        }
    }

    _getDates(start: Date, end: Date): IGridDate [] { 
        let startGrid = start.toGridDate();
        let endGrid = end.toGridDate();
        let dates = [startGrid];
        while(!startGrid.equals(endGrid)) {
            startGrid = startGrid.add({days: 1});
            dates.push(startGrid);
        }
        return dates;
    }

    formatTimetable(timetable: ITimetable) {
        let formated= new EventMap();
        if(timetable && timetable.events) {
            let typeMap = this._eventTypes.reduce((previous: Map<string,IEventType>, next: IEventType) =>{
                previous.set(next.identity, next);
                return previous;
            }, new Map())

            for(let event of timetable.events) {
                let type = typeMap.get(event.eventTypeId) || null;
                
                let dates = this._getDates(event.start, event.end);
                for(let date of dates) {
                    formated.addEvent(date, event, type);
                }
            }
        }
        return formated;
    }

    onWindowResize() {
        let width = window.innerWidth;
        if(width < 1000) {
            if(this.state.showGrid){
                this.setState({showGrid: false})
            }
        }
        else if(!this.state.showGrid){
            this.setState({showGrid: true})
        }
    }

    componentDidMount() {
        window.addEventListener('resize', this.onWindowResize.bind(this));
      }
    

    onDateChanged(date: IGridDate) {
        this.setState({current: date})
    }

    styleCell(date:IGridDate) {
        let events = this.state.events.getEvents(date);
        if(events && events.length) {
            let colour = events[0].type.colour
            let background = `rgba(${colour.r}, ${colour.g}, ${colour.b}, 0.40)`;
            let borderColour = `rgba(${colour.r}, ${colour.g}, ${colour.b})`;
            return {customStyle: {["background-color"]: background, ["border-color"]: borderColour }, customClass: 'grid-hasEvent'}
        }
    }

    _renderWithProfile(userProfile: IUserProfile) {
        let { isLoaded, showGrid, events } = this.state;
        if(isLoaded == false) {
            let service = new CalendarService(userProfile);
            Promise.all([
                service.timetable(this.state.current, this.state.current),
                service.eventTypes(),
                service.calendarLocale()
            ]).then(([timetable, eventTypes, locale]) => {
                this._locale = locale;
                this._eventTypes = eventTypes;
                this.setState({events: this.formatTimetable(timetable), isLoaded: true});
            });
        }

        return(
            <div className="calendar-main">
            {this.state.isLoaded &&
                <div className="datePicker corner-all noselect">
                    <DatePicker dateChanged={this.onDateChanged.bind(this)} calendarLocale={this._locale} date={this.state.current}/>
                    {
                        showGrid && 
                        <CalendarGrid dateChanged={this.onDateChanged.bind(this)} calendarLocale={this._locale} date={this.state.current} cellStyle={this.styleCell.bind(this)}/>
                    }
                    <EventList calendarLocale={this._locale} date={this.state.current} showMonth={showGrid} events={events}/>
                </div>
            }
            </div>
        );
    }

    render() {
        return (
            <SecurityContext.Consumer>
                {this._renderWithProfile.bind(this)}
            </SecurityContext.Consumer> 
        );

    }
}

export default Calendar;

