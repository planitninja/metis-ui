'use strict';
import * as React from 'react';
import { Component } from 'react';
import { IGridDate, ICalendarLocale } from './grid-date';

export interface ICalendarGridProps {
    date: IGridDate
    dateChanged?: (date: IGridDate) => void;
    calendarLocale: ICalendarLocale;
    cellStyle?: (date: IGridDate) => {customStyle: React.CSSProperties, customClass: string};
}

interface ICalendarGridState {
    current: IGridDate,
    days: string[];
    rows: IGridRow[]
}


interface IGridCell extends IGridDate {

}

interface IGridRow {
    cells: IGridCell[];
}


function buildDays(calendarLocale: ICalendarLocale): string[] {
    let day = calendarLocale.firstDayOfTheWeek;
    let retVal = [];
    for (let i = 0; i < 7; i++) {
        if (day >= 7) {
            day = 0;
        }
        retVal.push(calendarLocale.shortDayName(day));
        day++;
    }
    return retVal;
}

function buildRows(current: IGridDate, calendarLocale: ICalendarLocale): IGridRow[] {
    let first = current.gridMonth.firstDayOfMonth;
    let firstDayOfTheWeek = calendarLocale.firstDayOfTheWeek;
    let frontPadding = first.day - firstDayOfTheWeek;
    frontPadding = frontPadding < 0 ? frontPadding + 7 : frontPadding;
    let calendarDate = first.remove({ days: frontPadding });
    let rows = [];
    do {
        let cells = [];
        for (let i = 0; i < 7; i++) {
            cells.push(calendarDate);
            calendarDate = calendarDate.add({ days: 1 });
        }
        rows.push({ cells });
    }
    while(calendarDate.month == current.month) ;
    return rows;
}

class CalendarGrid extends Component<ICalendarGridProps, ICalendarGridState>
{
    _calendarLocale: ICalendarLocale;
    constructor(props: ICalendarGridProps, context: {}) {
        super(props, context);
        this._calendarLocale = props.calendarLocale;
    }

    static getDerivedStateFromProps(props: ICalendarGridProps, state: ICalendarGridState) {
        if (state == null || state.current == null || props.date.month !== state.current.month || props.date.year !== state.current.year) {
            return {
                current: props.date,
                rows: buildRows(props.date, props.calendarLocale),
                days: buildDays(props.calendarLocale)
            };
        }
        return null;
    }


    selectDate(date: IGridDate) {
        if(date.month == this.state.current.month) {
            this.setState({ current: date });
        }
        else {
            this.setState({ current: date, rows: buildRows(date, this._calendarLocale) });
        }
        if(this.props.dateChanged) {
            this.props.dateChanged(date);
        }
    }

    isCurrent(date: IGridDate) {
        return this.state.current.equals(date);
    }

    isWeekend(date: IGridDate) {
        return this._calendarLocale.isWeekend(date.day);
    }

    currentMonth(date: IGridDate) {
        return this.state.current.month == date.month;
    }

    heading(day: string) {
        let weekend = (day == 'sat' || day == 'sun');
        let className = weekend ? 'week-end' : '';

        return (<th scope="col" id="{day}" className={className}><span title={day}>{day}</span></th>);
    }

    cellStyle(cell: IGridCell) {
        let linkStyle:React.CSSProperties = {};
        let cellClass:string = '';
        let linkClass:string = '';
        let currentMonth = this.currentMonth(cell);
        if(currentMonth && this.props.cellStyle) {
            let custom = this.props.cellStyle(cell);
            if(custom) {
                linkStyle = custom.customStyle || {};
                linkClass = custom.customClass || '';
            }
        }
        cellClass = this.isWeekend(cell) ? 'week-end' : '';
        cellClass = currentMonth ? cellClass : cellClass + 'other-month';


        linkClass += ' default';
        if (this.isCurrent(cell)) {
            linkClass += ' current';
        }

        return {cellClass, linkStyle, linkClass};
    }

    cell(cell: IGridCell) {
        let gridDate = cell;
        let {cellClass, linkStyle, linkClass} = this.cellStyle(cell);

        return (
            <td className={cellClass} onClick={this.selectDate.bind(this, gridDate)}>
                <a className={linkClass} style={linkStyle}>{gridDate.date}</a>
            </td>);
    }

    row(row: IGridRow) {
        return (
            <tr>
                {row.cells.map(this.cell.bind(this))}
            </tr>);
    }

    render() {
        let { rows, days } = this.state;
        return (
            <table className="date-grid">
                <thead>
                    <tr>
                        {days.map(this.heading.bind(this))}
                    </tr>
                </thead>
                <tbody>
                    {rows.map(this.row.bind(this))}
                </tbody>
            </table >
        );
    }
}

export default CalendarGrid;
