'use strict';
import * as React from 'react';
import { Component } from 'react';
import { ICalendarLocale, IGridDate } from './grid-date';
import { ITimetableEvent, IEventType } from './api';


export interface IEventMap {
    hasEvents(date: IGridDate): boolean;
    getEvents(date: IGridDate): {event: ITimetableEvent, type: IEventType}[];
    addEvent(date: IGridDate, event: ITimetableEvent, type: IEventType): void;
}

export interface IEventListProps {
    date: IGridDate;
    calendarLocale: ICalendarLocale;
    showMonth: boolean;
    events? : IEventMap
}

interface IEventListState {
    cells: IGridDate[];
    current: IGridDate;
    showMonth: boolean;
}


function buildMonthRows(date: IGridDate) {
    let first = date.gridMonth.firstDayOfMonth;
    let last = date.gridMonth.lastDayOfMonth;
    let retVal = [];
    do {
        retVal.push(first);
        first = first.add({days:1});
    }
    while(!first.equals(last));
    return retVal;
}


function buildDayRow(date: IGridDate) {
    return [date];
}


class EventList extends Component<IEventListProps, IEventListState>
{
    _calendarLocale: ICalendarLocale;
    _listRef: any = {};
    constructor(props: IEventListProps, context: {}) {
        super(props, context);
        this._listRef = React.createRef();
        this._calendarLocale = props.calendarLocale;
    }

    static getDerivedStateFromProps(props: IEventListProps, state: IEventListState) {
        if (state == null || state.current == null || state.showMonth != props.showMonth) {
            return {
                current: props.date,
                cells: props.showMonth ? buildMonthRows(props.date) : buildDayRow(props.date),
                showMonth: props.showMonth
            }
        }

        if(props.showMonth) {
            if(props.date.month !== state.current.month || props.date.year !== state.current.year) {
                return {
                    current: props.date,
                    cells: buildMonthRows(props.date),
                    showMonth: props.showMonth
                }
            }
        }
        else {
            if(!props.date.equals(state.current)) {
                return {
                    current: props.date,
                    cells: buildDayRow(props.date),
                    showMonth: props.showMonth
                }
            }
        }

        return null;
    }

    componentDidUpdate(prevProps: IEventListProps) {
        if(prevProps.date == null || !this.props.showMonth) {
            return;
        }
        if(prevProps.date.month !== this.props.date.month || prevProps.date.year !== this.props.date.year) {
            return;
        }
        if(prevProps.date.date !== this.props.date.date) {
            this.scrollTo(this.props.date);
        }     
    }

    scrollTo(gridDate: IGridDate){
        let key = `${gridDate.month}-${gridDate.date}`;
        let ref = this._listRef[key];
        if(ref) {
            ref.current.scrollIntoView();
        }
    }

    formatDay(gridDate: IGridDate) {
        return this._calendarLocale.formatDate(gridDate);
    }

    typeStyle(eventType: IEventType):any {
        let colour = eventType.colour;
        let borderColour = `rgba(${colour.r}, ${colour.g}, ${colour.b})`;
        let backgroundColor = `rgba(${colour.r}, ${colour.g}, ${colour.b}, 0.3)`;
        return {
            "border-color": borderColour,
            "background-color": backgroundColor
        }
    }

    events(gridDate: IGridDate) {
        if(this.props.events && this.props.events.hasEvents(gridDate)) {
            let events = this.props.events.getEvents(gridDate);

            return events.map((e) => (
                <div className="list-event" style={this.typeStyle(e.type)}>
                    <div>{this._calendarLocale.formatDateTime(e.event.start)} - {this._calendarLocale.formatDateTime(e.event.end)}</div>
                    <div>{e.type.name}</div>
                </div>
            ));
        }
    }

    renderDay(gridDate: IGridDate) {
        let key = `${gridDate.month}-${gridDate.date}`;
        let ref:any = React.createRef();
        this._listRef[key] = ref;
        return (
            <div className="corner-all event-item" ref={ref}>
                <div>{this.formatDay(gridDate)}</div>
                {
                    this.events(gridDate)
                }
            </div>
        )
    }

    render() {
        let {cells} =this.state;
        this._listRef = {};
        return (
            <div className="event-list">
                {cells.map(this.renderDay.bind(this))}
            </div >
        );
    }
}

export default EventList;
