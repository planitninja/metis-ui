import { ICalendarLocale, IGridDate } from "./grid-date";


export interface ILocaleData {
    weekends: number [];
    shortDayNames: string[];
    monthNames: string[];
    firstDayOfTheWeek: number;
    dateFormat: string;
    eventTimeFormat: string;
}

const defaultLocaleDate:ILocaleData = {
    weekends: [0, 6],
    shortDayNames: ['Sun', 'Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'],
    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    firstDayOfTheWeek: 0,
    dateFormat: "DD MMMM YYYY",
    eventTimeFormat: "DD-MM-YY hh:mm"
}


interface IFormatData {
    year: number;
    month: number;
    date: number;
    day: number;
    hour: number;
    minute: number;
}

const regEx = /MM(MM)?|hh|mm|DD|YY(YY)?/gi;
const tokens: {[index:string]: (gridDate: IFormatData, locale: ICalendarLocale)=>string} = {
    "hh" : (gridDate: IFormatData, locale: ICalendarLocale)=> {
        let date = gridDate.hour.toString();
        date = date.length == 1? '0' + date : date;
        return date;
    },
    "mm" : (gridDate: IFormatData, locale: ICalendarLocale)=> {
        let date = gridDate.minute.toString();
        date = date.length == 1? '0' + date : date;
        return date;
    },
    "DD" : (gridDate: IFormatData, locale: ICalendarLocale)=> {
        let date = gridDate.date.toString();
        date = date.length == 1? '0' + date : date;
        return date;
    },
    "MMMM" : (gridDate: IFormatData, locale: ICalendarLocale)=> {
        return locale.monthName(gridDate.month);
    },
    "MM" : (gridDate: IFormatData, locale: ICalendarLocale)=> {
        let date = gridDate.month.toString();
        date = date.length == 1? '0' + date : date;
        return date;
    },
    "YY" : (gridDate: IFormatData, locale: ICalendarLocale)=> {
        return gridDate.year.toString().substring(2);
    } 
    ,
    "YYYY" : (gridDate: IFormatData, locale: ICalendarLocale)=> {
        return gridDate.year.toString();
    }    
}

export default class implements ICalendarLocale{

    _localeData: ILocaleData;
    constructor() {
        this._localeData = defaultLocaleDate;
    }

    _applyFormat(date: IFormatData, pattern: string) {

        let result = pattern;
        let reg = new RegExp(regEx);   
        var matches = pattern.match(reg);
        for(let match of matches) {
            if(tokens[match]) {
                result = result.replace(match, tokens[match](date,this));
            }
            else {
                result = result.replace(match, '');
            }
        }
        return result;
    }

    formatDate(gridDate: IGridDate): string {
        let data = {
            year: gridDate.year,
            month: gridDate.month,
            date: gridDate.date,
            day: gridDate.day,
            hour: 0,
            minute: 0
        };
        return this._applyFormat(data, this._localeData.dateFormat);
    }    

    formatDateTime(date: Date): string {
        let data = {
            year: date.getFullYear(),
            month: date.getMonth(),
            date: date.getDate(),
            day: date.getDay(),
            hour: date.getHours(),
            minute: date.getMinutes()
        };
        return this._applyFormat(data, this._localeData.eventTimeFormat);
    }    

    isWeekend (day: number) {
        return this._localeData.weekends.indexOf(day) >= 0;
    } 
    shortDayName (day: number){
        return this._localeData.shortDayNames[day];
    }
    monthName (month: number){
        return this._localeData.monthNames[month];
    }
    get firstDayOfTheWeek(): number {
        return this._localeData.firstDayOfTheWeek;
    }


}