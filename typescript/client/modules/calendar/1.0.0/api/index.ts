import { SecuredRequest, ISecuredRequest, IUserProfile } from '../../../../core/access-control';
import { ICalendarLocale, IGridDate } from '../grid-date';
import CalendarLocale from '../calendar-locale';
const Version = "1.0.0";
const ServiceName = "timetable";

export interface ITimetableEvent {
    identity: string;
    eventTypeId: string;
    start: Date;
    end: Date
}

export interface ITimetable {
    events : ITimetableEvent[];
}

export interface IEventType {
    identity: string;
    name: string;
    colour: {r: number, g: number, b: number};
}

export interface ICalendarService {
    timetable(start: IGridDate, end: IGridDate) : Promise<ITimetable>;
    eventTypes(): Promise<IEventType[]>;
    calendarLocale() :Promise<ICalendarLocale>;
}


function FakeEvents(start: IGridDate) {
    let {year, month} = start;
    let events = [
        {identity: "one", eventTypeId: "one", start: new Date(year, month, 4, 9, 30), end: new Date(year, month, 4, 15, 45)},
        {identity: "two", eventTypeId: "two", start: new Date(year, month, 6, 10, 30), end: new Date(year, month, 6, 14, 30)},
        {identity: "three", eventTypeId: "three", start: new Date(year, month, 8, 20, 30), end: new Date(year, month, 9, 9, 30)},
        {identity: "four", eventTypeId: "one", start: new Date(year, month, 9, 15, 0), end: new Date(year, month, 9, 17, 0)}
    ]
    return events;
}


export default class implements ICalendarService {
    _securedRequest: ISecuredRequest;
    constructor(profile: IUserProfile) {
        this._securedRequest = new SecuredRequest(profile);
    }

    _urlBuilder() {
        return `${Version}/${ServiceName}`        
    }

    
    timetable(start: IGridDate, end: IGridDate) : Promise<ITimetable> {
        let url = `/calendar`;
        return this._securedRequest.fetch<ITimetable>('calendar', '/', {}).then(result => {
            if(result && result.events) {
                for(let event of result.events) {
                    event.start = new Date(event.start),
                    event.end = new Date(event.end)
                }
            }
            return result;
        }); 
    }

    eventTypes() : Promise<IEventType[]> {
        return Promise.resolve([
            {identity: 'one', name: 'event1', colour: {r: 27 ,g: 115, b: 26}},
            {identity: 'two', name: 'event2', colour: {r: 154 ,g: 36, b: 36}},
            {identity: 'three', name: 'event3', colour: {r: 66 ,g: 128, b: 241}}
        ]);
    }

    calendarLocale() :Promise<ICalendarLocale> {
        return Promise.resolve(new CalendarLocale());
    }
}