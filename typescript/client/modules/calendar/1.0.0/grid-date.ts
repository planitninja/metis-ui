declare global {
    interface Date {
        toGridDate(): IGridDate;
    }
}

Date.prototype.toGridDate = function (): IGridDate {
    return new GridDate(this);
};

export interface IGridMonth {
    firstDayOfMonth: IGridDate;
    lastDayOfMonth: IGridDate;
}

export interface IDuration {
    days?: number;
    months?: number;
    years?: number;
}

export interface IGridDate {
    day: number;
    date: number;
    month: number;
    year: number;
    gridMonth: IGridMonth;
    equals(gridDate: IGridDate): boolean;
    add(duration: IDuration): IGridDate;
    remove(duration: IDuration): IGridDate;
    toDate(): Date;
}

export interface ICalendarLocale {
    formatDate(gridDate: IGridDate): string;
    formatDateTime(date: Date): string;
    isWeekend: (day: number) => boolean;
    shortDayName: (day: number) => string;
    monthName: (month: number) => string;
    firstDayOfTheWeek: number;
}

class GridMonth implements IGridMonth {
    firstDayOfMonth: IGridDate;
    lastDayOfMonth: IGridDate;
    constructor(date: IGridDate) {
        this.firstDayOfMonth = new Date(date.year, date.month, 1).toGridDate();
        this.lastDayOfMonth = new Date(date.year, date.month + 1, 0).toGridDate();
    }

}


class GridDate implements IGridDate {
    day: number;
    date: number;
    month: number;
    year: number;
    constructor(date: Date) {
        this.day = date.getDay();
        this.date = date.getDate();
        this.month = date.getMonth();
        this.year = date.getFullYear();
    }

    equals(gridDate: IGridDate): boolean {
        return this.date == gridDate.date &&
            this.month == gridDate.month &&
            this.year == gridDate.year;
    }

    get gridMonth(): IGridMonth {
        return new GridMonth(this);
    }

    add(duration: IDuration): IGridDate {
        let {days, months, years } = duration;
        days = days || 0;
        months = months || 0;
        years = years || 0;
        return new Date(this.year + years, this.month + months, this.date + days).toGridDate();
    }
    remove(duration: IDuration): IGridDate {
        let {days, months, years } = duration;
        days = days || 0;
        months = months || 0;
        years = years || 0;
        return new Date(this.year - years, this.month - months, this.date - days).toGridDate();
    }

    toDate(): Date {
        return new Date(this.year, this.month, this.date);
    }
}


export default Date;