'use strict';
import * as React from 'react';
import { Component } from 'react';
import { AuthorisedComponent } from '../../core/access-control';
import VersionSwitchComponent from '../../core/version-switch';
import V1 from './1.0.0';

const featureName = 'calendar';

const versionMap = (version: string) => {
    return (<V1/>)
}

class CalendarComponent extends Component<{}, {}>
{
    constructor(props: {}, context: {}) {
        super(props, context);
    }

    render() {
        return (
            <AuthorisedComponent feature={featureName}>
                <VersionSwitchComponent feature={featureName} versionMap={versionMap} />
            </AuthorisedComponent>
        );
    }
}

export default CalendarComponent;
