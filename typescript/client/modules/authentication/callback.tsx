'use strict';
import * as React from 'react';
import { Component } from 'react';
import { withRouter,  RouteComponentProps } from 'react-router-dom'
import { UnauthorisedError } from '../../errors';
import { ITokenData, Constants } from '../../core/access-control';

let css = require('./style.css');


interface ICallbackState {
}

interface ICallbackProperties extends RouteComponentProps{
}

function splitHash(hash: string) {
    if(hash.startsWith('#')) {
        hash = hash.substr(1);
    }
    let parts = hash.split('&');
    let data: ITokenData = {
        now: new Date().getTime()
    };
    for(let part of parts) {
        let [key, value] = part.split('=');
        switch(key) {
            case 'state':
                data.state = value;
                break;
            case 'id_token':
                data.idToken = value;
                break;
            case 'expires_in': 
                data.expiresIn = value;
                break;
            case 'token_type': 
                data.type = value;
                break;
            case 'access_token':
                data.accessToken = value;
                break;
        }
    }
    if(data.accessToken || data.idToken) {
        return data;
    }
    return null;
}


/**
 * The notification list UI component.
 */
class Callback extends Component<ICallbackProperties, ICallbackState>
{
    constructor(props: ICallbackProperties, context: {}) {
        super(props, context);
        let hash = this.props.location.hash;
        let hashData =splitHash(hash);
        if(hashData) {
            let raw = sessionStorage.getItem(hashData.state);
            let data: {nonce: string, redirect: string} = raw ? JSON.parse(raw) : null;
            if(data) {
                sessionStorage.removeItem(hashData.state);
                if(this._validateToken(hashData)) {
                    localStorage.setItem(Constants.userNameKey, 'john_sut_1@yahoo.co.uk');
                    delete hashData.state;
                    sessionStorage.setItem(Constants.tokenKey, JSON.stringify(hashData));
                    if(data.redirect) {
                        window.location.assign(data.redirect);
                        return;
                    }
                }
            }
        }
    }

    _validateToken(data: ITokenData) {
        return true;
    }

    render() {
        return (
            <div className="main">
                <UnauthorisedError></UnauthorisedError>
            </div>
        );

    }
}

export default withRouter(Callback);