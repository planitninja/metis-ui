'use strict';
import * as React from 'react';
import { Component } from 'react';
import { II18n } from '../../core/i18n';
import { AuthenticationAPI, IAuthenticationProvider, Schema, ILoginInformation } from './api';
import { AuthenticationProviders } from './authentication-providers';
import { ValidationControl } from '../../core/controls';
import { HostName } from '../../core/host';

interface ILoginState {
    i18n: II18n; 
    value: string; 
    authenticationProviders: IAuthenticationProvider[], 
    stage:number, 
    loading: boolean,
    recentLogins: ILoginInformation []
}

/**
 * The notification list UI component.
 */
class Login extends Component<{location?: any}, ILoginState>
{
    api: AuthenticationAPI;
    redirect: string;
    constructor(props: {}, context: {}) {
        super(props, context);
        this.api = new AuthenticationAPI();
        this.redirect = "/";
        if(this.props.location) {
            const params = new URLSearchParams(this.props.location.search);
            this.redirect = params.get('redirect') || "/";
        }
        this.state = {
            i18n: {
                get: (key) => key
            },
            stage: 0,
            recentLogins: this.api.recentLogins(),
            loading: false,
            value: null,
            authenticationProviders: []
        }
    }

    handleChange(value: any) {
        this.setState({ value });
    }

    nextCicked() {
        this.setState({loading: true});
        this.api.getProviders(this.state.value).then(result => {
            this.setState({stage: 1, loading: false, authenticationProviders: result});
        });
    }

    changeEmail() {
        this.setState({stage: 0});
    }


    stageOne(i18n: II18n) {
        let { value, loading } = this.state;
        return (
            <div className="form-signin">
                <div><h1>{i18n.get('auth.signin')}</h1></div>
                <div><ValidationControl showError={false} name="email" value={value} onValueChanged={this.handleChange.bind(this)}  schema={Schema.email} type="email" required autoComplete="on" autoFocus className="form-control" placeholder={i18n.get('email-address')} /> </div>
                <div className="form-action">
                    <button disabled={loading} className="btn btn-lg btn-primary btn-block" onClick={this.nextCicked.bind(this)}>{i18n.get('next')}</button>
                </div>
            </div>
        );
    }

    stageTwo(i18n: II18n) {
        let {value, authenticationProviders} = this.state;
        return (
            <div className="form-signin">
                <div><h1>{i18n.get('auth.signin')}</h1></div>
                <div>{value}</div>
                <AuthenticationProviders authenticationProviders={authenticationProviders} redirect={this.redirect}/>
                <div className="form-action">
                    <button className="btn btn-lg btn-primary btn-block" onClick={this.changeEmail.bind(this)}>{i18n.get('changeEmail')}</button>
                </div>
            </div>
        );
    }

    getState() {
        let { stage, i18n } = this.state;
        switch(stage) {
            case 0:
                return this.stageOne(i18n);
            case 1:
                return this.stageTwo(i18n);
            default:
                return this.stageOne(i18n);
        }
    }

    quickLogin(email:string) {
        window.location.assign(`${HostName}/auth/callback#nonce=store`);
    }

    recentLogin(loginInformation: ILoginInformation) {
        return(<div onClick={this.quickLogin.bind(this, loginInformation.email)}>{loginInformation.email}</div>)
    }

    render() {
        let recentLogins = this.state.recentLogins || [];
        return (
            <div className="main">
                <div>
                    {recentLogins.map(this.recentLogin.bind(this))}
                </div>
                { this.getState() }
            </div>
        );

    }
}

export default Login;