import { FieldTypes } from '@metis/model-validation';

export default  {
    email: { 
        type: FieldTypes.text, 
        required: {allowEmpty: false}, 
        email:{} 
    }
};