
import Schema from './schema';

export interface IAuthenticationProvider {
    name: string;
    hint: string;
    id: string;
    icon: string;
}

export interface ILoginInformation {
    email: string
}

export class AuthenticationAPI {
    constructor() {
    }

    recentLogins() :ILoginInformation [] {
        return [
            {email: "john_sut_1@yahoo.co.uk"},
            {email: "john.sutcliffe@opuscapita.com"}
        ]
    }

    getProviders(email: string): Promise<IAuthenticationProvider[]> {

        return fetch("http://localhost:4005/1.0.0/authentication-provider/" + email, {
            headers: {
                "Content-Type": "application/json"
            },
            method: "GET"
        }).then(response => {
            return response.json();
        })
    }

    authenticate(provider: IAuthenticationProvider, redirect: string):Promise<any> {

        return fetch(`http://localhost:4005/1.0.0/authentication-provider/authenticate/${provider.id}?hint=${provider.hint}&redirect=${redirect}`, {
            headers: {
                "Content-Type": "application/json"
            },
            method: "GET"
        }).then(response => {
            return response.json();
        })
    }
}

export {Schema};