'use strict';
import * as React from 'react';
import { Component } from 'react';
let css = require('./style.css');
import { IAuthenticationProvider, AuthenticationAPI } from './api';
/**
 * The notification list UI component.
 */
class AuthenticationProviders extends Component<{ authenticationProviders: IAuthenticationProvider[], redirect: string }, { authenticationProviders: IAuthenticationProvider[] }>
{
    api: AuthenticationAPI
    constructor(props: { authenticationProviders: IAuthenticationProvider[], redirect: string }, context: {}) {
        super(props, context);
        this.api = new AuthenticationAPI();
        this.state = {
            authenticationProviders: props.authenticationProviders
        }
    }


    static getDerivedStateFromProps(nextProps: { authenticationProviders: IAuthenticationProvider[] }, prevState: { authenticationProviders: IAuthenticationProvider[] }) {
        if (nextProps.authenticationProviders !== prevState.authenticationProviders) {
            return {
                authenticationProviders: nextProps.authenticationProviders
            };
        } else return null;
    }

    onClicked(provider: IAuthenticationProvider) {
        this.api.authenticate(provider, this.props.redirect).then(result => window.location = result.redirect);
    }

    renderProvider(provider: IAuthenticationProvider) {
        return (<div  onClick={this.onClicked.bind(this, provider )}>{provider.name}</div>)
    }

    render() {
        let { authenticationProviders } = this.state;
        if (authenticationProviders) {
            return (
                <div>
                    {authenticationProviders.map(p => this.renderProvider(p))}
                </div>
            );
        }
        return (null);
    }
}


export { AuthenticationProviders };