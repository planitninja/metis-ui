'use strict';
import * as React from 'react';
import { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Login from './login';
import Callback from './callback';
let css = require('./style.css');

class AuthenticationRouting extends Component<{}, {}>
{
    constructor(props: {}, context: {}) {
        super(props, context);
    }


    render() {

        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/auth/callback" component={Callback}/>
                    <Route path="/" component={Login}/>
                </Switch>
            </BrowserRouter>
        );
    }
}


export default AuthenticationRouting;