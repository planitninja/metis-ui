export default {
    authentication: (version) =>
        import( /* webpackChunkName: "authentication" */ `./authentication`),
    calendar: () =>
        import( /* webpackChunkName: "calendar" */ `./calendar`)
}