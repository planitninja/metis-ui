'use strict';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import MainPage from './main-page';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import ModuleRoute from './core/module-routing';


ReactDOM.render((
    <BrowserRouter>
        <Switch>
                <ModuleRoute path="/auth*" module="authentication" />
                <Route path="/" component={MainPage}/>
        </Switch>
    </BrowserRouter>
    
), document.getElementById('root'));