import * as express from 'express';
import * as http from 'http';
import * as https from 'https';
import * as fs from 'fs';
import {Request, Response} from 'express';
let app = express();
const port = 3000;

function init() {
    let staticFolder = `${__dirname}/static`;
    app.use('/static', express.static(staticFolder));
    app.use('/', (req:Request, res:Response) => res.sendFile(`${__dirname}/static/index.html`));


    var privateKey = fs.readFileSync('key.pem');
    var certificate = fs.readFileSync('certificate.pem');
    
    var options = {key: privateKey, cert: certificate};

    http.createServer(app).listen(port);
    https.createServer(options, app).listen(3001);
    console.log(`Listening on port ${port}`)
}
(()=>init())();
