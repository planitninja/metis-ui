const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractCssChunks = require("extract-css-chunks-webpack-plugin");

const {
  resolve
} = require("path");

module.exports = {
  entry: {
    app: resolve(__dirname, "typescript/client/index.js")
  },

  output: {
    path: resolve(__dirname, './src/server/static'),
    filename: "[name].bundle.js",
    chunkFilename: "[name].chunk.js",
    publicPath: "/static/"
  },

  node: {
    net: 'empty',
    tls: 'empty',
    dns: 'empty'
  },

  resolve: {
    modules: ['node_modules', resolve('app'), ]
  },

  module: {
    rules: [{
        test: /\.css$/,
        use: [
          ExtractCssChunks.loader,
          {
            loader: "css-loader",
            options: {modules: false}
          }
        ]

      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192
            }
          }
        ]
      }
    ]
  },
  plugins: [ 
    new ExtractCssChunks(
      {
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: "[name].css",
        chunkFilename: "[name].css",
        hot: true // optional as the plugin cannot automatically detect if you are using HOT, not for production use

      }
  ),
    new HtmlWebpackPlugin({
      inject: true,
      template: './assets/index.html',
      filename: 'index.html'
    })
  ]
}